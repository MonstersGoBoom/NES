
PPUderle
         clc
PPUderleloop
         ldy #0
         lda (get),y
         beq PPUendderle       ;eof

         ;if positive -> simply copy
         ;if negative -> multiple bytes

         bpl PPUderlecopy

         and #$7f         ;number of multiple bytes.
         tax
         inx
         iny
         lda (get),y      ;read byte
         dey
PPUderleunpack
				sta PPU_DATA
;         sta (put),y      ;and write unpacked bytes
         iny
         dex
         bpl PPUderleunpack

         tya              ;update put pointer
         ;clc
         adc put
         sta put
         bcc +
         inc put+1
         clc
+
         lda get          ;update get pointer
         adc #2
         sta get
         bcc PPUderleloop
         clc
         inc get+1
         bcc PPUderleloop
PPUderlecopy
         tax
         inc get          ;increase get pointer
         bne +
         inc get+1
+
PPUderlecopyloop
         lda (get),y      ;read  uncompressed bytes
				 sta PPU_DATA
;         sta (put),y      ;write uncompressed bytes
         iny
         dex
         bne PPUderlecopyloop

         tya            ;update get pointer
         ;clc
         adc get
         sta get
         bcc +
         inc get+1
         clc
+
         tya            ;update put pointer
         adc put
         sta put
         bcc PPUderleloop
         clc
         inc put+1
         bcc PPUderleloop
PPUendderle
         rts
