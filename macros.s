macro pshall
  pha
  txa
  pha
  tya
  pha
ENDM

macro popall
  pla
  tay
  pla
  tax
  pla
endm

;	adds a single sprite to OAM data  
;	subtracts 4 in both axis to center it 

macro sprite _x,_y,_flags,_id 

	pshall
	ldy #$00 
	lda _y 
	sta (spr_next),y
	inc spr_next
	lda _id 
	sta (spr_next),y
	inc spr_next
	lda _flags
	sta (spr_next),y
	inc spr_next
	lda _x
	sta (spr_next),y
	inc spr_next
	popall

endm 


;	set PPU address
macro setPPU addr 
	lda #>addr 
	sta PPU_ADDRESS
	lda #<addr
	sta PPU_ADDRESS
endm

;	copy data to PPU 
macro copyPPU input,vram,size 
	setPPU vram 
	mov16im input,$2
	ldx #>size 
	ldy #<size
	jsr _PPU_COPY
endm

macro VBL 
-	bit PPU_STATUS
  bpl -
endm 

;	set out to #in 
macro mov16im in,out
	lda #<in
	sta out
	lda #>in
	sta out+1
endm

; set out to in 
macro mov16 in,out
	lda in
	sta out
	lda in+1
	sta out+1
endm

;	add #value to from store in dest
.MACRO add16im from,value,dest 
	clc
	lda from
	adc #<value
	sta dest 
	lda from+1
	adc #>value 
	sta dest+1
.ENDM

.MACRO sub16im from,value,dest 
	sec
	lda from
	sbc #<value
	sta dest 
	lda from+1
	sbc #>value
	sta dest+1
.ENDM

;	add value to from store in dest
.MACRO add16 from,value,dest 
	clc
	lda from
	adc value
	sta dest 
	lda from+1
	adc value+1
	sta dest+1
.ENDM

;	add value to from store in dest
.MACRO sub16 from,value,dest 
	sec
	lda from
	sbc value
	sta dest 
	lda from+1
	sbc value+1
	sta dest+1
.ENDM
