INESPRG 1
INESCHR 0
;NES2PRGRAM 4
INESMAP 4

; pad code by NesDoug 
; RLE and unpack by WVL https://csdb.dk/release/?id=34685

enum $10
	temp1		db 0
	temp2		db 0
	temp3		db 0
	temp4		db 0
	temp5		db 0
	temp6		db 0
	temp7		db 0
	temp8		db 0
	get     dw 0 
	put     dw 0 

	pad1		.db 0 ;these must be 1 then 2, to work with controller read
	pad2		.db 0
	pad1_new	.db 0
	pad2_new	.db 0

	waiting     db 0 
	evenframe		db 0 

	; player info
	player.x 		dw 0
	player.vx 	dw 0 
	player.y 		dw 0
	player.vy		dw 0 
	player.id 	db 0
	player.life db 0 
	player.angle db 0
	player.flags db 0 
	player.thrust db 0
	player.xthrust dw 0 
	player.ythrust dw 0 
	player.char db 0 

	orb.x 		dw 0
	orb.vx 	dw 0 
	orb.y 		dw 0
	orb.vy		dw 0 
	orb.picked db 0

	spr_next		dw 0 

	meta_xpos 	db 0
	meta_ypos 	db 0
	meta_xoff 	db 0
	meta_yoff 	db 0
	meta_tile 	db 0
	meta_flags 	db 0
	meta_flag 	db 0 
	meta_base		dw 0
	; room for N bullets
	cparticle			db 0 
	bullets_x 	dsb 32
	bullets_y 	dsb 32 
	bullets_l		dsb 32 
	bullets_a		dsb 32 

	guns_x			dsb 4
	guns_y			dsb 4
	guns_a			dsb 4
	guns_clock	dsb 4
	guns_reset 	dsb 4

ende
enum $300
	screenoff dsb 32
ende

.include "nes.s"
.include "macros.s"

.org $c000 
RESET:
;	init NES 

  sei
  CLD
  ldx #$FF
  txs
  inx
  stx PPU_CONTROL
  stx PPU_MASK
  stx $4010
  ldx #$40
  stx $4017

	VBL 

; clear memory
-
	lda #$00
  sta $0000, x
  sta $0100, x
  sta $0200, x
  sta $0300, x
  sta $0400, x
  sta $0500, x
  sta $0600, x
  sta $0700, x
  inx
  bne -

;	set up screenoffsets
	ldx #$00 
	mov16im #$2000,$2
-
	lda $2
	sta screenoff,x 
	inx 
	lda $3
	sta screenoff,x 
	inx 
	add16im $2,32,$2
	cpx #40
	bne -


	lda #$0
	sta player.life
	mov16im #$8000,player.x 
	mov16im #$8000,player.y 
	lda #$20
	sta player.id 

	copyPPU palette,VRAM_TILE_CLUT,#$10 
	copyPPU palette,VRAM_SPR_CLUT,#$10 
;	copyPPU tiles,VRAM_TILEBANK0,#$800

	mov16im #$8000,player.x 
	mov16im #$8000,player.y 

	; unpack the room 
	mov16im room_01,$2

	ldy #$00
	;	copy address of rle map 
	lda ($2),y 
	sta $6
	iny 
	lda ($2),y
	sta $7
	iny 	
	;	Player
	lda ($2),y 
	sta player.x+1 
	iny 
	lda ($2),y 
	sta player.y+1 
	iny 
	;	Orb
	lda ($2),y 
	sta orb.x+1 
	iny 
	lda ($2),y 
	sta orb.y+1 
	iny 

	;	Guns
	ldx #$00 
-
	lda ($2),y 
	sta guns_x,x  
	iny 
	lda ($2),y 
	sta guns_y,x  
	iny 
	lda ($2),y 
	sta guns_a,x  
	iny 
	lda ($2),y 
	sta guns_reset,x  
	iny 
	sta guns_clock,x 
	inx 
	cpx #$4 
	bne -

;	unpack RLE 
;	setPPU $2000 
;	mov16 $6,get
;	mov16im $0,put
;	jsr derle
;	jsr _PPU_UNRLE

;	mov16 $6,$2
;	mov16im $400,$8
;	jsr _MEM_UNRLE


	setPPU $2000
	lda PPU_DATA


;	vblank

	mov16im tiles,get 
	mov16im #$000,put 
	setPPU VRAM_TILEBANK0
	jsr PPUderle


	mov16im title,get 
	mov16im #$000,put 
	setPPU VRAM_NAMETABLE_0
	jsr PPUderle


;	unpacktoPPU title,VRAM_NAMETABLE_0


  lda #PPU_NMI_VBLANK
  sta PPU_CONTROL

	lda #PPU_BG_ON | PPU_SPR_ON 
  sta PPU_MASK

	VBL 

loop:
  inc waiting
;	psuedo vblank , wait for NMI to trigger
-
	lda waiting
  bne -

	jsr pad_poll

	lda pad1_new
	and #PAD_START
	beq loop

  lda #0
  sta PPU_CONTROL
  sta PPU_MASK

	setPPU $2000 
	mov16 $6,get
	mov16im $0,put
	jsr PPUderle

  lda #PPU_NMI_VBLANK
  sta PPU_CONTROL

	lda #PPU_BG_ON | PPU_SPR_ON 
  sta PPU_MASK


game_loop
  inc waiting
;	psuedo vblank , wait for NMI to trigger
-
	lda waiting
  bne -

	jsr pad_poll


	ldy spr_next

	mov16im #$200,spr_next

;	clear sprites that weren't used last frame
	lda #$00 
-	
	sta (spr_next),y 
	iny
	bne -


	lda player.life
	bne dead
	;	player control 
	jsr player_control
	jsr orb_control
	jsr gun_control
	jmp alive
dead:
	lda evenframe 
	sta $2
	lda #$50
	sta $3
	ldx #$00 
goverloop
	clc  
	lda GAMEOVER,x
	sbc #6
	sta $4
	sprite $2,$3,#$00,$4
	clc 
	lda $2
	adc #$8
	cpx #$3 
	bne +
	adc #$8
+
	sta $2 
	
	inx 
	cpx #$9
	bne goverloop

	lda evenframe
	and #$3
	bne alive
	lda player.life
	and #$f
	ora #$10

	ldx player.x+1
	ldy player.y+1
	jsr new_bullet
	clc
	lda player.life 
	adc #$1
	sta player.life
	
alive:
	jsr player_draw

	;	bullets

	lda evenframe
	and #$1 
	tax

-	
	lda bullets_l,x 
	cmp #$00
	beq nodraw

	sec
	sbc #$1
	sta bullets_l,x 

	lda bullets_a,x 
	and #$f
	tay 

	clc 
	lda bullets_x,x 
	adc player_xthrust,y 
	sta bullets_x,x
	sta $2

	clc 
	lda bullets_y,x 
	adc player_ythrust,y 
	sta bullets_y,x
	sta $3

	lda #$17
	sta $4

	lda bullets_a,x 
	and #$20
	beq +
	lda #$2e
	sta $4
+	
	sprite $2,$3,#0,$4

nodraw
	inx 
	inx
	cpx #$20 
	bmi -

	lda evenframe
	and #1
;	beq + 
	lda #PPU_BG_ON | PPU_SPR_ON | PPU_BG8px | PPU_SPR8px
  sta PPU_MASK
	jmp ++
+
;	lda #PPU_BG_ON | PPU_SPR_ON | PPU_EM_GREEN | PPU_BG8px | PPU_SPR8px
;  sta PPU_MASK
++


	jmp game_loop
	rts

gun_control:
.if 1
	ldx #$00 
-
	stx $5
	lda guns_x,x 
	cmp #$ff 
	beq +
	sec
	lda guns_clock,x
	sbc #$1
	sta guns_clock,x
	cmp #$00 
	bne +
	lda guns_reset,x
	sta guns_clock,x

	lda guns_x,x
	sta $3
	lda guns_y,x 
	sta $4
	lda guns_a,x
	sta $2
	jsr new_bullet_234
+
	ldx $5
	inx 
	cpx #$4
	bne -
.endif
	rts 

orb_control:
	lda orb.x+1
	sta $2
	lda orb.y+1
	sta $3
	sprite $2,$3,#0,#$7

	lda orb.picked
	beq orbyout

;	lda player.thrust
;	bne +
	add16	orb.x,player.xthrust,orb.x
	add16	orb.y,player.ythrust,orb.y

	add16 orb.x,orb.vx,orb.x 
	add16 orb.y,orb.vy,orb.y 
	add16im orb.vy,1,orb.vy
orbyout	 
	rts

;		x hi y lo ($2) source
_PPU_COPY:
	; save y 
	;
	sty $4
	ldy #$00
-
	lda ($2),y 
	sta PPU_DATA
	iny 
	cpy $4
	bne - 
	cpx #0 
	beq +

	ldy #$00 
	sty $4
	inc $3
	dex 
	bne -
+	
	rts 

.include "derle.s"

; base frames
player_frames: 	hex 18 19 1a 1b 1c 1b 1a 19 18 19 1a 1b 1c 1b 1a 19 18  
;	flips 
player_flags:		hex 00 00 00 00 00 80 80 80 80 c0 c0 c0 c0 40 40 40 40 
;	dx 
player_ythrust:	hex fe fe fe ff 
player_xthrust:	hex 00 01 02 02 02 02 02 01 00 ff fe fe fe fe fe ff 00 
; dy 

player_control:

	mov16im 0,player.xthrust
	mov16im 0,player.ythrust

  lda #$01
  sta $4016
  lda #$00
  sta $4016	

	sta player.thrust
	lda player.angle
	lsr 
	lsr 
	and #15 
	tay 


	lda pad1
	and #PAD_A

 ; lda $4016 ; A
 ; and #%00000001
  beq nothrust 

	lda #$00 
	sta $3
	clc
	lda player_xthrust,y 
	sta $2
	cmp #$80
	bmi +
	lda #$ff 
	sta $3
+
	add16 player.vx,$2,player.vx 
	mov16 $2,player.xthrust

	lda #$00 
	sta $3
	clc
	lda player_ythrust,y 
	sta $2
	cmp #$80
	bmi +
	lda #$ff 
	sta $3
+

	add16 player.vy,$2,player.vy 
	mov16 $2,player.ythrust

	inc player.thrust
nothrust

	lda pad1_new
	and #PAD_B
  beq noshoot

	pshall
	;	y is the angle to shoot
	tya 
	ldx player.x+1
	ldy player.y+1 
	jsr new_bullet
	popall

noshoot

	lda pad1
	and #PAD_LEFT
  beq noleft 
	dec player.angle
noleft
	lda pad1
	and #PAD_RIGHT
  beq noright
	inc player.angle
noright

	lda player_frames,y 
	sta player.id
	lda player_flags,y 
	sta player.flags

	lda #$00 
	sta meta_flag

	lda player.char
	beq nodie
	; check for the safe tile 
	cmp #$40
	bne die
	;	check we're facing up 
	; so we can land
	lda player.angle
	lsr 
	lsr 

	and #$7
	cmp #$0 
	bne die
	; if we're thrusting don't reset vy 
	lda player.thrust
	bne +	
	mov16im #$0000,player.vy
+
	mov16im #$0000,player.vx
	jmp nodie

die:


	lda player.life 
	bne noreset
	lda #$01 
	sta player.life

noreset
	;turn red
	lda #$02 
	sta meta_flag
nodie

	;	apply movement 
	add16 player.x,player.vx,player.x
	add16 player.y,player.vy,player.y 
	add16im player.vy,$1,player.vy 
	rts 

player_draw:
	lda #<metasprite_pointers
	sta $2 
	lda #>metasprite_pointers
	sta $3
	clc
	lda player.x+1
	adc #4
	sta meta_xpos
	clc
	lda player.y+1
	adc #4
	sta meta_ypos

	lda #$00
	sta $4
	lda player.thrust
	beq +
	lda evenframe
	and #1
;	lsr 
	sta $4
+	
	lda player.angle
	lsr 
	lsr 
	and #15 
	asl 
	clc
	adc $4
	
	
;	lda #10 
	asl 
	tay 
	lda ($2),y 
	sta meta_base
	iny 
	lda ($2),y
	sta meta_base+1
	jsr meta_sprite_draw
	rts 


new_bullet:
	sta $2
	stx $3
	sty $4
new_bullet_234
	lda cparticle
	and #$1f 
	tax 
	lda bullets_l,x 
;	cmp #8
;	bpl +
	lda $3
	sta bullets_x,x 
	lda $4
	sta bullets_y,x 
	lda $2
	sta bullets_a,x

	lda #$30 
	sta bullets_l,x 
	inc cparticle
+
	rts 

IRQ:
	rti
NMI:
	pshall
  lda #$00
  sta OAM_ADDRESS ; reset the SPR-RAM write offset
  lda #$02
  sta $4014 ; start the DMA transfer from $0200

	inc evenframe

	lda player.x+1
	lsr 
	lsr
	lsr
	and #$1f
	tax

	lda player.y+1
	lsr 
	lsr
	lsr
	and #$1f
	tay
	iny 
	jsr screenget
	sta player.char


	lda #$20 
	sta PPU_ADDRESS
	lda #$20
	sta PPU_ADDRESS

	lda player.vx+1
	jsr hex2ppu
	lda player.vx
	jsr hex2ppu
	lda #$00 
	sta PPU_DATA

	lda player.vy+1
	jsr hex2ppu
	lda player.vy
	jsr hex2ppu
	lda #$00 
	sta PPU_DATA




	lda player.char
	sta PPU_DATA
	jsr hex2ppu
  
	lda #$00
	sta PPU_SCROLL
	lda #$00
	sta PPU_SCROLL
	
  lda #$00
  sta waiting
	popall
  RTI

hex2ppu
	tay 
	lsr 
	lsr
	lsr
	lsr
	and #$f 
	clc
	adc #$50
	sta PPU_DATA
	tya 
	and #$f 
	clc
	adc #$50
	sta PPU_DATA
	rts 

meta_sprite_draw:
	ldx #$00
meta_sprite_inner:
	ldy #00
	lda (meta_base),y 
	cmp #$80 
	beq no_more 
	sta meta_xoff
	iny
	lda (meta_base),y 
	sta meta_yoff
	iny
	lda (meta_base),y 
	sta meta_tile
	iny
	lda (meta_base),y 
	sta meta_flags 

	;	 yposition
	ldy #$00
	clc 
	lda meta_ypos
	adc meta_yoff
	sta (spr_next),y 
	iny
	; tile 	
	lda meta_tile
	sta (spr_next),y
	iny 

	; flags
	lda meta_flags 
	ora meta_flag 
	sta (spr_next),y
	iny 
	; xpos 
	clc
	lda meta_xpos
	adc meta_xoff
	sta (spr_next),y

	clc 
	lda meta_base
	adc #$4 
	sta meta_base
	lda meta_base+1
	adc #0
	sta meta_base+1

	clc 
	lda spr_next
	adc #$4 
	sta spr_next
	inx 
	bne meta_sprite_inner
no_more:
	rts


screenget
;  bit PPU_STATU
	stx $2
	tya 
	asl 
	tax	
	inx
	lda screenoff,x 
	sta PPU_ADDRESS
	dex
	clc 
	lda $2
	adc screenoff,x
	sta PPU_ADDRESS
	lda PPU_DATA
	rts 


pad_poll: ;modified, reads both controllers
	ldy #0
	jsr pad_poll_sub
	ldy #1
	jsr pad_poll_sub
	rts


pad_poll_sub:
	lda pad1, y
	sta temp5

	ldx #0

@padPollPort:

	lda #1
	sta $4016
	lda #0
	sta $4016
	lda #8
	sta temp1

@padPollLoop:

	lda $4016,y
	lsr a
	ror temp2,x ;3 reads temp2,3,4
	dec temp1
	bne @padPollLoop

	inx
	cpx #3
	bne @padPollPort

	lda temp2
	cmp temp3
	beq @done
	cmp temp4
	beq @done
	lda temp3

@done:

	sta pad1,y
	tax
	eor temp5
	and pad1,y
	sta pad1_new,y
	rts	

palette:	incbin "data\thrust2.pal"
title:	incbin "data\title.rlz"

;level_00:	incbin "data\l1.rlz"
;level_01:	incbin "data\l2.rlz"
level_02:	incbin "data\l3.rlz"
;level_03:	incbin "data\l3.rlz"

room_01:
					.dw level_02
					.db $10,27 	;	player location 
					.db $78,$d7		;	globe 
					.db $b8,$a0,$9,$23 ;	gun0 
					.db $58,$a0,$5,$48 ;	gun1 
					.db $ff,$ff,$ff,$ff ; gun2
					.db $ff,$ff,$ff,$ff ; gun3

					include "data\ship.asm"
;level_04:	incbin "data\l3.rle"
;level_05:	incbin "data\l3.rle"
;level_06:	incbin "data\l3.rle"
;level_07:	incbin "data\l3.rle"

GAMEOVER:
					db "gameover"

tiles:		incbin "data\thrust.chr.rlz"
lastbyte:	db 0 

.org $FFFA  ; vector table
	.dw NMI
	.dw RESET
	.dw IRQ
