PPU_CONTROL EQU $2000
PPU_MASK 		EQU $2001
PPU_STATUS 	EQU $2002
OAM_ADDRESS EQU $2003
OAM_DATA 		EQU $2004
PPU_SCROLL 	EQU $2005
PPU_ADDRESS EQU $2006
PPU_DATA 		EQU $2007

PAD_A = $01 
PAD_B = $02
PAD_SELECT = $04
PAD_START = $08
PAD_UP = $10
PAD_DOWN = $20
PAD_LEFT = $40
PAD_RIGHT = $80


VRAM_TILEBANK0 	EQU $0000
VRAM_TILEBANK1 	EQU $1000
VRAM_NAMETABLE_0 EQU $2000
VRAM_NAMETABLE_1 EQU $2400
VRAM_NAMETABLE_2 EQU $2800
VRAM_NAMETABLE_3 EQU $2c00

VRAM_TILE_CLUT 	EQU $3f00
VRAM_SPR_CLUT 	EQU $3f10

;BGRs bMmG
;|||| ||||
;|||| |||+- Greyscale (0: normal color, 1: produce a greyscale display)
;|||| ||+-- 1: Show background in leftmost 8 pixels of screen, 0: Hide
;|||| |+--- 1: Show sprites in leftmost 8 pixels of screen, 0: Hide
;|||| +---- 1: Show background
;|||+------ 1: Show sprites
;||+------- Emphasize red
;|+-------- Emphasize green
;+--------- Emphasize blue
PPU_GREYSCALE EQU 1<<0
PPU_BG8px 		EQU 1<<1
PPU_SPR8px 		EQU 1<<2
PPU_BG_ON 		EQU 1<<3
PPU_SPR_ON 		EQU 1<<4
PPU_EM_RED 		EQU 1<<5
PPU_EM_GREEN	EQU 1<<6
PPU_EM_BLUE 	EQU 1<<7

OAM_FLIP_V		EQU $80
OAM_FLIP_H		EQU $40

PPU_NMI_VBLANK EQU 1<<7
;|         |    D7: Execute NMI on VBlank                             |
;|         |           0 = Disabled                                   |
;|         |           1 = Enabled                                    |
PPU_MASTER_SLAVE EQU 1<<6
;|         |    D6: PPU Master/Slave Selection --+                    |
;|         |           0 = Master                +-- UNUSED           |
;|         |           1 = Slave               --+                    |
PPU_SPRITE_SIZE EQU 1<<5
;|         |    D5: Sprite Size                                       |
;|         |           0 = 8x8                                        |
;|         |           1 = 8x16                                       |
PPU_BG_ADDRESS EQU 1<<4
;|         |    D4: Background Pattern Table Address                  |
;|         |           0 = $0000 (VRAM)                               |
;|         |           1 = $1000 (VRAM)                               |
PPU_SPR_ADDRESS EQU 1<<3
;|         |    D3: Sprite Pattern Table Address                      |
;|         |           0 = $0000 (VRAM)                               |
;|         |           1 = $1000 (VRAM)                               |
PPU_INCREMENT_32 EQU	1<<2
;|         |    D2: PPU Address Increment                             |
;|         |           0 = Increment by 1                             |
;|         |           1 = Increment by 32                            |

;|         | D1-D0: Name Table Address                                |
;|         |         00 = $2000 (VRAM)                                |
;|         |         01 = $2400 (VRAM)                                |
;|         |         10 = $2800 (VRAM)                                |
;|         |         11 = $2C00 (VRAM)                                |
;+---------+----------------------------------

