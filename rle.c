#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main(int argc, char *argv[])
{
  FILE *in;
  FILE *out;
  int data[65536];
  int packeddata[65536];
  int counter,counter2=0;
  int samebytes=0;
  int bytes=0;

  if (argc==3){
    counter=0;
    in = fopen (argv[1],"rb");    // open file
    if (in==NULL){printf("Can't find %s, aborting.",argv[1]);}
    else{
      while(feof(in)==0) {
        data[counter]=fgetc(in);
        counter++;
      }// while
      counter--;                           // don't count eof byte
      bytes=counter;
      fclose(in);                          // close stream
      data[counter]=1+data[counter-1];     // small bug-fix to make sure the test A doesnt go wrong

      //pack file

      counter =0;
      counter2=0;
      while(counter<bytes){
        samebytes=0;
        //check if there's a serie of same bytes
        while (samebytes<128 && data[counter]==data[counter+1] && counter<bytes){
          samebytes++;
          counter++;
        }// while

        //if samebytes==0, there was no serie :(, so we check how many bytes until the next serie
        if (samebytes!=0){
          packeddata[counter2]=128+samebytes-1;
          packeddata[counter2+1]=data[counter];
          counter2+=2;
          counter++;
        }
        else {
          //packeddata[counter2]=0;
          counter2++;
          while (samebytes<127 && !(samebytes==126 && data[counter]==data[counter+1]) && !(data[counter]==data[counter+1] && data[counter]==data[counter+2]) && counter<bytes){ //test A
            samebytes++;
            packeddata[counter2]=data[counter];
            counter++;
            counter2++;
          }//while
          packeddata[counter2-samebytes-1]=samebytes;
        }//else
      }//while bytes left to pack

      //postfilter : here we filter the output a bit to optimize depacking speed..
      //we are looking for :
      // 1) blabla $80 $XX blabla
      // if blabla is nonpacked data, and uses less than $7e bytes, we can merge the $80,$xx in that data
      // this saves changing loops in the depacker and doesnt cost extra bytes
      // 2) blabla $81 $XX blabla
      // and we check if it's possible to merge the blabla, $81 $xx and blabla together in unpacked data
      // 3) somehow, due to filtering we did, we get 2 uncompressed ranges after another, so we check if we can add them

      bool filtered=true;
      bool filterednow;
      int nextcommand;
      int nextnextcommand;

      while(filtered){
       filtered=false;
       counter=0;
       while(packeddata[counter]!=0){
        filterednow=false;
        if (packeddata[counter]<128)     {nextcommand    =counter+1+packeddata[counter];        }
        else                             {nextcommand    =counter+2;                            }
        if (packeddata[counter]==0)      {nextcommand    =counter;                              }
        if (packeddata[nextcommand]<128) {nextnextcommand=nextcommand+1+packeddata[nextcommand];}
        else                             {nextnextcommand=nextcommand+2;                        }
        if (packeddata[nextcommand]==0)  {nextnextcommand=nextcommand;                          }

        //check for case 1 (merge $80 with $01-$7d)

        if (packeddata[nextcommand]==128 && packeddata[counter]<126){
         filterednow=filtered=true;
         packeddata[counter]=packeddata[counter]+2;
         packeddata[nextcommand]=packeddata[nextcommand+1];
        }//if
        if (packeddata[counter]==128 && packeddata[nextcommand]<126){
         filterednow=filtered=true;
         packeddata[counter]=packeddata[nextcommand]+2;
         packeddata[nextcommand]=packeddata[counter+1];
        }//if

        //check for case 2 (merge $01-$7f + $81 + $01-$7f)

        if (packeddata[nextcommand]==128+1 && packeddata[counter]<128 && packeddata[counter]!=0 && packeddata[nextnextcommand]<128 && packeddata[nextnextcommand]!=0){
         if ((packeddata[counter]+3+packeddata[nextnextcommand])<128){
          packeddata[counter]=packeddata[counter]+packeddata[nextnextcommand]+3;
          packeddata[nextcommand+2]=packeddata[nextcommand]=packeddata[nextcommand+1];
          filterednow=filtered=true;
         }//if
        }//if

        // check for case 3

         // i didnt put this in yet, not 100% sure how to do it yet (and to keep $80/$81 mixing correctly..)

        // next command, except if we did filtering, in which case we check again at the same position!
        if (!filterednow){
          if (packeddata[counter]<128)     {counter=counter+1+packeddata[counter];        }
          else                             {counter=counter+2;                            }
        }//if
        filterednow=false;

       }//while
      }//while filtered

      //output file :)
      out=fopen(argv[2],"wb");

      for(counter=0;counter<(counter2);counter++){
        fputc(packeddata[counter],out);
      }//for
      fputc(0,out); // write end byte
      fclose(out);
    }//else
  }//if
  else {printf ("usage : rle [infile] [outfile]");}//else
  return 0;
}
