metasprite_0_data:

	.byte -4,-4,$18,0
	.byte 128

metasprite_1_data:

	.byte -4,-4,$18,0
	.byte -4,  4,$20,2
	.byte 128

metasprite_2_data:

	.byte -4,-4,$19,0
	.byte 128

metasprite_3_data:

	.byte -4,-4,$19,0
	.byte -5,  3,$23,2
	.byte 128

metasprite_4_data:

	.byte -4,-3,$1a,0
	.byte 128

metasprite_5_data:

	.byte -4,-3,$1a,0
	.byte -10,  3,$22,2
	.byte 128

metasprite_6_data:

	.byte -3,-3,$1b,0
	.byte 128

metasprite_7_data:

	.byte -3,-3,$1b,0
	.byte -10,-3,$21,2
	.byte 128

metasprite_8_data:

	.byte -3,-3,$1c,0
	.byte 128

metasprite_9_data:

	.byte -3,-3,$1c,0
	.byte -11,-3,$24,2
	.byte 128

metasprite_10_data:

	.byte -3,-5,$1b,0|OAM_FLIP_V
	.byte 128

metasprite_11_data:

	.byte -3,-5,$1b,0|OAM_FLIP_V
	.byte -10,-5,$21,2|OAM_FLIP_V
	.byte 128

metasprite_12_data:

	.byte -4,-5,$1a,0|OAM_FLIP_V
	.byte 128

metasprite_13_data:

	.byte -4,-5,$1a,0|OAM_FLIP_V
	.byte -10,-11,$22,2|OAM_FLIP_V
	.byte 128

metasprite_14_data:

	.byte -4,-4,$19,0|OAM_FLIP_V
	.byte 128

metasprite_15_data:

	.byte -4,-4,$19,0|OAM_FLIP_V
	.byte -5,-11,$23,2|OAM_FLIP_V
	.byte 128

metasprite_16_data:

	.byte -4,-4,$18,0|OAM_FLIP_V
	.byte 128

metasprite_17_data:

	.byte -4,-4,$18,0|OAM_FLIP_V
	.byte -4,-12,$20,2|OAM_FLIP_V
	.byte 128

metasprite_18_data:

	.byte -4,-4,$19,0|OAM_FLIP_H|OAM_FLIP_V
	.byte 128

metasprite_19_data:

	.byte -4,-4,$19,0|OAM_FLIP_H|OAM_FLIP_V
	.byte -3,-11,$23,2|OAM_FLIP_H|OAM_FLIP_V
	.byte 128

metasprite_20_data:

	.byte -4,-5,$1a,0|OAM_FLIP_H|OAM_FLIP_V
	.byte 128

metasprite_21_data:

	.byte -4,-5,$1a,0|OAM_FLIP_H|OAM_FLIP_V
	.byte   2,-11,$22,2|OAM_FLIP_H|OAM_FLIP_V
	.byte 128

metasprite_22_data:

	.byte -5,-5,$1b,0|OAM_FLIP_H|OAM_FLIP_V
	.byte 128

metasprite_23_data:

	.byte -5,-5,$1b,0|OAM_FLIP_H|OAM_FLIP_V
	.byte   2,-5,$21,2|OAM_FLIP_H|OAM_FLIP_V
	.byte 128

metasprite_24_data:

	.byte -5,-5,$1c,0|OAM_FLIP_H|OAM_FLIP_V
	.byte 128

metasprite_25_data:

	.byte -5,-5,$1c,0|OAM_FLIP_H|OAM_FLIP_V
	.byte   3,-5,$24,2|OAM_FLIP_H|OAM_FLIP_V
	.byte 128

metasprite_26_data:

	.byte -5,-3,$1b,0|OAM_FLIP_H
	.byte 128

metasprite_27_data:

	.byte -5,-3,$1b,0|OAM_FLIP_H
	.byte   2,-3,$21,2|OAM_FLIP_H
	.byte 128

metasprite_28_data:

	.byte -4,-3,$1a,0|OAM_FLIP_H
	.byte 128

metasprite_29_data:

	.byte -4,-3,$1a,0|OAM_FLIP_H
	.byte   2,  3,$22,2|OAM_FLIP_H
	.byte 128

metasprite_30_data:

	.byte -4,-4,$19,0|OAM_FLIP_H
	.byte 128

metasprite_31_data:

	.byte -4,-4,$19,0|OAM_FLIP_H
	.byte -3,  3,$23,2|OAM_FLIP_H
	.byte 128

metasprite_pointers:

	.word metasprite_0_data
	.word metasprite_1_data
	.word metasprite_2_data
	.word metasprite_3_data
	.word metasprite_4_data
	.word metasprite_5_data
	.word metasprite_6_data
	.word metasprite_7_data
	.word metasprite_8_data
	.word metasprite_9_data
	.word metasprite_10_data
	.word metasprite_11_data
	.word metasprite_12_data
	.word metasprite_13_data
	.word metasprite_14_data
	.word metasprite_15_data
	.word metasprite_16_data
	.word metasprite_17_data
	.word metasprite_18_data
	.word metasprite_19_data
	.word metasprite_20_data
	.word metasprite_21_data
	.word metasprite_22_data
	.word metasprite_23_data
	.word metasprite_24_data
	.word metasprite_25_data
	.word metasprite_26_data
	.word metasprite_27_data
	.word metasprite_28_data
	.word metasprite_29_data
	.word metasprite_30_data
	.word metasprite_31_data

