# cobbled together really fast
CC = gcc

.PHONY: all 

all: safe

safe:
	$(CC) -Wall asm6f.c -o asm6f
	$(CC) rle.c -o rle
	rle data\l1.bin data\l1.rlz
	rle data\l2.bin data\l2.rlz
	rle data\l3.bin data\l3.rlz
	rle data\title.bin data\title.rlz
	rle data\thrust.chr data\thrust.chr.rlz
	asm6f.exe -l -m -c base.s base.nes

